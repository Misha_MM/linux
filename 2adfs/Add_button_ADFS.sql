INSERT INTO auth.providers 
VALUES (5,'AD FS 2','oauth2','adfs',1,'{}');

INSERT INTO auth.social_manifest (provider_id,app_name,version,payload)
VALUES (5,'android','2.0.0','{
  "dp_size": 48,
  "networks": [
    {
      "features": {
        "login3": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=json_alert"
      },
      "pictures": {
        "24": "{server}/images/adfs-color-icons/icon_24x24.png",
        "36": "{server}/images/adfs-color-icons/icon_36x36.png",
        "48": "{server}/images/adfs-color-icons/icon_48x48.png",
        "64": "{server}/images/adfs-color-icons/icon_64x64.png",
        "72": "{server}/images/adfs-color-icons/icon_72x72.png",
        "96": "{server}/images/adfs-color-icons/icon_96x96.png",
        "128": "{server}/images/adfs-color-icons/icon_128x128.png",
        "144": "{server}/images/adfs-color-icons/icon_144x144.png"
      }
    }
  ]
}'),
(5,'android','2.2.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-color-icons/icon-web.svg",
      "features": {
        "login3": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=json_alert"
      },
      "pictures": "{server}/images/adfs-color-icons/icon-web.svg"
    }
  ]
}'),
(5,'ios','3.4.0','{
  "networks": [
    {
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form",
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form",
        "login3": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      },
      "pictures": {
        "96": "{server}/images/adfs-white-icons/icon_96x96.png"
      }
    }
  ]
}'),
(5,'linux','8.0.0','{
  "networks": [
    {
      "features": {
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      },
      "pictures": {
        "24": "{server}/images/adfs-white-icons/icon.svg",
        "36": "{server}/images/adfs-white-icons/icon.svg",
        "48": "{server}/images/adfs-white-icons/icon.svg",
        "64": "{server}/images/adfs-white-icons/icon.svg",
        "72": "{server}/images/adfs-white-icons/icon.svg",
        "96": "{server}/images/adfs-white-icons/icon.svg",
        "144": "{server}/images/adfs-white-icons/icon.svg"
      }
    }
  ]
}'),
(5,'linux','8.3.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon.svg",
      "features": {
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      }
    }
  ]
}'),
(5,'macos','8.0.0','{
  "networks": [
    {
      "features": {
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      },
      "pictures": {
        "24": "{server}/images/adfs-white-icons/icon.svg",
        "36": "{server}/images/adfs-white-icons/icon.svg",
        "48": "{server}/images/adfs-white-icons/icon.svg",
        "64": "{server}/images/adfs-white-icons/icon.svg",
        "72": "{server}/images/adfs-white-icons/icon.svg",
        "96": "{server}/images/adfs-white-icons/icon.svg",
        "128": "{server}/images/adfs-white-icons/icon.svg",
        "144": "{server}/images/adfs-white-icons/icon.svg"
      }
    }
  ]
}'),
(5,'macos','8.3.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon.svg",
      "features": {
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      }
    }
  ]
}'),
(5,'plugin_outlook_desktop','1.0.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon_72x72.png",
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form",
        "logout": "{server}/api/v4/oauth2/v2/{provider_id}/logout?response=hidden_form"
      }
    }
  ]
}'),
(5,'plugin_outlook_web','1.0.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon.svg",
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=post_msg&relative_url={relative_url}",
        "logout": "{server}/api/v4/oauth2/v2/{provider_id}/logout?response=post_msg"
      }
    }
  ]
}'),
(5,'plugin_r7_office','1.0.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon.svg",
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=post_msg&relative_url=https://{domain}",
        "logout": "{server}/api/v4/oauth2/v2/{provider_id}/logout?response=post_msg"
      }
    }
  ]
}'),
(5,'plugin_thunderbird','1.0.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon.svg",
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=post_msg&relative_url=https://{domain}",
        "logout": "{server}/api/v4/oauth2/v2/{provider_id}/logout?response=post_msg"
      }
    }
  ]
}'),
(5,'web','5.2.9','{
  "networks": [
    {
      "icon": "{server}/images/adfs-color-icons/icon-web.svg",
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=post_msg&relative_url={relative_url}",
        "logout": "{server}/api/v4/oauth2/v2/{provider_id}/logout?response=post_msg"
      },
      "pictures": "{server}/images/adfs-color-icons/icon-web.svg"
    }
  ]
}'),
(5,'windows','7.2.0','{
  "networks": [
    {
      "features": {
        "login": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form",
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form",
        "login3": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      },
      "pictures": {
        "24": "{server}/images/adfs-color-icons/icon_24x24.png",
        "36": "{server}/images/adfs-color-icons/icon_36x36.png",
        "48": "{server}/images/adfs-color-icons/icon_48x48.png",
        "64": "{server}/images/adfs-color-icons/icon_64x64.png",
        "72": "{server}/images/adfs-color-icons/icon_72x72.png",
        "96": "{server}/images/adfs-color-icons/icon_96x96.png",
        "128": "{server}/images/adfs-color-icons/icon_128x128.png",
        "144": "{server}/images/adfs-color-icons/icon_144x144.png"
      }
    }
  ]
}'),
(5,'windows','8.0.0','{
  "networks": [
    {
      "features": {
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      },
      "pictures": {
        "24": "{server}/images/adfs-white-icons/icon.svg",
        "36": "{server}/images/adfs-white-icons/icon.svg",
        "48": "{server}/images/adfs-white-icons/icon.svg",
        "64": "{server}/images/adfs-white-icons/icon.svg",
        "72": "{server}/images/adfs-white-icons/icon.svg",
        "96": "{server}/images/adfs-white-icons/icon.svg",
        "144": "{server}/images/adfs-white-icons/icon.svg"
      }
    }
  ]
}'),
(5,'windows','8.3.0','{
  "networks": [
    {
      "icon": "{server}/images/adfs-white-icons/icon.svg",
      "features": {
        "login2": "{server}/api/v4/oauth2/v2/{provider_id}/authorize?response=hidden_form"
      }
    }
  ]
}')