# System-wide .bashrc file for interactive bash(1) shells.


# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
# but only if not SUDOing and have SUDO_PS1 set; then assume smart user.
if ! [ -n "${SUDO_USER}" -a -n "${SUDO_PS1}" ]; then
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

# enable bash completion in interactive shells
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
	function command_not_found_handle {
	        # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
		   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
		   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
		else
		   printf "%s: command not found\n" "$1" >&2
		   return 127
		fi
	}
fi



alias ls='ls --color=auto'
alias ll='ls -lFo'
alias UP='sudo apt update ; sudo apt full-upgrade -y ; sudo apt autoclean ; sudo autoremove -y'
alias conf1='cd /opt/trueconf/server/bin/vcs ; ./tc_regkey set "WebManager" "enable_public_conference_scheduling" str "1"'
alias conf0='cd /opt/trueconf/server/bin/vcs ; ./tc_regkey set "WebManager" "enable_public_conference_scheduling" str "0"'

alias manager='sudo systemctl status trueconf-manager'
alias rstmanager='sudo systemctl restart trueconf-manager'

alias sip_btn1='cd /opt/trueconf/server/bin/vcs ; ./tc_regkey set "AppProperties" "show_sip_number_btn" str "1"'
alias sip_btn0='cd /opt/trueconf/server/bin/vcs ; ./tc_regkey set "AppProperties" "show_sip_number_btn" str " "'

alias rstbd='cd /opt/trueconf/server/etc/database ; systemctl restart postgresql.service'

alias vpn1='sudo systemctl start wg-quick@wg100-marinich2.service'
alias vpn0='sudo systemctl stop wg-quick@wg100-marinich2.service'

alias tru='systemctl --type=service | grep trueconf'
