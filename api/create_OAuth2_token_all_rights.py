#!/usr/bin/python3
# -*- coding: UTF-8 -*-


# Скрипт для создания OAuth2 приложения со всеми правами и access_token-а на основе API OAuth2 приложения 
# На выходе скрипт записывает токен в файл Token_OAuth2_all_rights.txt


import requests
import urllib3
from loguru import logger
import sys

logger.remove()
logger.add(sys.stdout, format="{time:YYYY-MM-DD HH:mm:ss:SSS} | {level} | [ {file}:{function}:{line} ] {message}")

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Создание API OAuth2 приложения
logger.info("Создание API OAuth2 приложения со всеми правами")
create_api_oauth2_applications = f"http://localhost/api/v3/api-clients"

create_api_oauth2_applications_payload = {
    "client_name": "API_OAuth2_all_rights",
    "redirect_uri": "https://localhost",
    "scope": "api.access_tokens api.clients api.clients.access_tokens conferences conferences.invitations conferences.invitations:read conferences.invitations:write conferences.participants users users.addressbook aliases:read aliases:write groups groups.users gateway.sip gateway.sip.peers gateway.h323 gateway.h323.peers gateway.sip:write gateway.sip:read gateway.sip.peers:write gateway.sip.peers:read gateway.h323:write gateway.h323:read gateway.h323.peers:write gateway.h323.peers:read gateway.transcoding:read gateway.transcoding:write gateway.rtp:read gateway.rtp:write gateway.webrtc:read gateway.webrtc:write directory:read directory:write directory.servers:read directory.servers:write enterprise.servers:read enterprise.servers:write enterprise:read enterprise:write server.license:read logs.messages:read logs.events:read logs.settings:read logs.records:read logs.records:write logs.calls:read logs.calls.participants:read logs.calls.invites:read conferences:read conferences:write conferences.records conferences.records:read conferences.records:write conferences.participants:read conferences.participants:write groups:read groups:write groups.users:read groups.users:write users:read users:write users.avatar:read users.avatar:write users.addressbook:read users.addressbook:write templates.conferences:read templates.conferences:write conferences.client_rights:read conferences.client_rights:write conferences.video_layouts:read conferences.video_layouts:write conferences.video_layouts.personal:read conferences.video_layouts.personal:write conferences.operators:read conferences.operators:write conferences.broadcast_presets:read conferences.broadcast_presets:write conferences.sessions:read conferences.sessions.participants:read conferences.sessions.podiums.participants:read conferences.sessions.podiums.participants:write settings:read settings:write documents:write"
 }


response = requests.post(create_api_oauth2_applications, json=create_api_oauth2_applications_payload, verify=False)
response_json = response.json()
id_applications = response_json.get("client", {}).get("id") # Получаем ID приложения
client_secret = response_json.get("client", {}).get("client_secret") # Получаем секретный ключ приложения


#print(id_applications)
#print(client_secret)

# Создаём access_token на основе API OAuth2 приложения
#logger.info("Создание access_token на основе API OAuth2 приложения")
token_url = f"https://localhost/oauth2/v1/token"
token_payload = {
    "grant_type": "client_credentials",
    "client_id": f"{id_applications}",
    "client_secret": f"{client_secret}"
}


response = requests.post(token_url, data=token_payload, verify=False)
#print(response.text)
response_json = response.json()
access_token = response_json.get("access_token") # Получаем токен 

#logger.info("Запись токена в файл Token_OAuth2_all_rights.txt")
with open("Token_OAuth2_all_rights.txt", "w", encoding='utf-8') as file:
    try:
        #logger.info("Токен записался в файл")
        file.write(access_token)
    except:
        #logger.info("В файл не записался токен")
        print("В файл не записался токен")

print(f"\nAccess Token: {access_token}")
