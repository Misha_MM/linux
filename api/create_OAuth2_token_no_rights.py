#!/usr/bin/python3
# coding=<utf8>


# Скрипт для создания OAuth2 приложения со всеми правами и access_token-а на основе API OAuth2 приложения 
# На выходе скрипт записывает токен в файл Token_OAuth2_all_rights.txt


import requests
import urllib3
from loguru import logger
import sys

logger.remove()
logger.add(sys.stdout, format="{time:YYYY-MM-DD HH:mm:ss:SSS} | {level} | [ {file}:{function}:{line} ] {message}")


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Создание API OAuth2 приложения
logger.info("Создание API OAuth2 приложения без прав")
create_api_oauth2_applications = f"http://localhost/api/v3/api-clients"

create_api_oauth2_applications_payload = {
    "client_name": "API_OAuth2_no_rights",
    "redirect_uri": "https://localhost",
    "scope": ""
 }


response = requests.post(create_api_oauth2_applications, json=create_api_oauth2_applications_payload, verify=False)
response_json = response.json()
id_applications = response_json.get("client", {}).get("id") # Получаем ID приложения
client_secret = response_json.get("client", {}).get("client_secret") # Получаем секретный ключ приложения


#print(id_applications)
#print(client_secret)


# Создаём access_token на основе API OAuth2 приложения 
token_url = f"https://localhost/oauth2/v1/token"
token_payload = {
    "grant_type": "client_credentials",
    "client_id": f"{id_applications}",
    "client_secret": f"{client_secret}"
}


response = requests.post(token_url, data=token_payload, verify=False)
#print(response.text)
response_json = response.json()
access_token = response_json.get("access_token") # Получаем токен 


with open("Token_OAuth2_no_rights.txt", "w", encoding="utf-8") as file:
    try:
        file.write(access_token)
    except:
        print("В файл не записался токен")

print(f"\nAccess Token: {access_token}")
